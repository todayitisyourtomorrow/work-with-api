import { useState } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const [val, setVal] = useState("");
  const [view, setView] = useState([]);
  const [val2, setVal2] = useState("");

  const generate = () => {
    axios
      .get(
        `https://api.fastforex.io/fetch-multi?from=${val}&to=${val2},EUR&api_key=b8a0bfee0a-29df098832-ru6d9h`
      )
      .then((response) => {
        setView(
          Object.keys(response.data.results).map((key) => ({
            [key]: response.data.results[key],
          }))
        );
      })
      .catch((error) => {
        console.log(error);
      });

  };

  return (
    <div className="container">
      <div className="input-block">
        <div className="input-wrapper">
          <input
            type="text"
            className="input"
            value={val}
            onChange={(event) => setVal(event.target.value)}
            placeholder="Input currency..."
          />
          <input
            type="text"
            className="input"
            value={val2}
            onChange={(event) => setVal2(event.target.value)}
            placeholder="Input another value..."
          />
          
          <button onClick={generate} className="button">
            Generate
          </button>
        </div>
        <div className="result-block">
          {view.map((item, index) => (
            <div key={index}>
              {Object.keys(item).map((key) => (
                <p key={key}>
                  {key}: {item[key]}
                </p>
              ))}
            </div>
          ))}

        </div>
      </div>
    </div>
  );
}

export default App;
